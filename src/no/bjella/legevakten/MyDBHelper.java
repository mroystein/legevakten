package no.bjella.legevakten;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class MyDBHelper extends SQLiteOpenHelper {
    // If you change the database schema, you must increment the database version.
    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "legevakter.db";
    
    
    private static final String TEXT_TYPE = " TEXT";
    private static final String REAL_TYPE = " REAL";
    private static final String COMMA_SEP = ",";
   /* private static final String SQL_CREATE_ENTRIES =
        "CREATE TABLE " + LegevaktDBEntry.TABLE_NAME + " (" +
        LegevaktDBEntry._ID + " INTEGER PRIMARY KEY," +
        LegevaktDBEntry.COLUMN_NAME_ENTRY_ID + TEXT_TYPE + COMMA_SEP +
        LegevaktDBEntry.COLUMN_NAME_NAME + TEXT_TYPE + COMMA_SEP +
       // Any other options for the CREATE command
        " )";*/
    
    private static final String DATABASE_CREATE = "create table "
    	      + LegevaktDBEntry.TABLE_NAME + "(" + LegevaktDBEntry._ID
    	      + " integer primary key autoincrement, " + 
    	      LegevaktDBEntry.COLUMN_NAME_NAME+ " text not null"+COMMA_SEP+
    	      LegevaktDBEntry.COLUMN_NAME_LATITUDE+TEXT_TYPE+COMMA_SEP+
    	       LegevaktDBEntry.COLUMN_NAME_LONGITUDE+TEXT_TYPE+COMMA_SEP+
    	       LegevaktDBEntry.COLUMN_NAME_OPENINGHOURS+TEXT_TYPE+COMMA_SEP+
    	       LegevaktDBEntry.COLUMN_NAME_PHONE+TEXT_TYPE+COMMA_SEP+
    	       LegevaktDBEntry.COLUMN_NAME_ADDR_POSTNAME+TEXT_TYPE+COMMA_SEP+
    	       
    	        LegevaktDBEntry.COLUMN_NAME_ADDR_POSTNR+TEXT_TYPE+COMMA_SEP+
    	         LegevaktDBEntry.COLUMN_NAME_ADDR_STREET+TEXT_TYPE+COMMA_SEP+
    	          LegevaktDBEntry.COLUMN_NAME_WEB+TEXT_TYPE+COMMA_SEP+
    	           LegevaktDBEntry.COLUMN_NAME_OPENING_COMMENT+TEXT_TYPE+COMMA_SEP+
    	            LegevaktDBEntry.COLUMN_NAME_COUNTRYCODE+TEXT_TYPE+COMMA_SEP+
    	             LegevaktDBEntry.COLUMN_NAME_MUNICODE+TEXT_TYPE+
    	       
    	      
    	      ");";

    private static final String SQL_DELETE_ENTRIES =
        "DROP TABLE IF EXISTS "+LegevaktDBEntry.TABLE_NAME; // FEIL?????

    
    public MyDBHelper(Context context) {
    	 super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(DATABASE_CREATE);
    }
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // This database is only a cache for online data, so its upgrade policy is
        // to simply to discard the data and start over
        db.execSQL(SQL_DELETE_ENTRIES);
        onCreate(db);
    }
    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onUpgrade(db, oldVersion, newVersion);
    }
    
    
}