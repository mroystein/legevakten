package no.bjella.legevakten;

public class Legevakt {
	
	String HealthServiceDisplayName;
	String HealthServiceLatitude;
	String HealthServiceLongitude;
	String VisitAddressPostName;
	
	String VisitAddressPostNr;
	String VisitAddressStreet;
	String HealthServiceWeb;
	String OpeningHours;
	String OpeningHoursComment;
	
	String HealthServicePhone;
	String AppliesToCountyCodes;
	String AppliesToMunicipalityCodes;
	
	int id;
	
	@Override
	public String toString() {
		return "" + HealthServiceDisplayName;
	}
	
	public String toString2() {
		return "Legevakt [HealthServiceDisplayName="
				+ HealthServiceDisplayName + ", HealthServiceLatitude="
				+ HealthServiceLatitude + ", HealthServiceLongitude="
				+ HealthServiceLongitude + ", VisitAddressPostName="
				+ VisitAddressPostName + ", VisitAddressPostNr="
				+ VisitAddressPostNr + ", VisitAddressStreet="
				+ VisitAddressStreet + ", HealthServiceWeb="
				+ HealthServiceWeb + ", OpeningHours=" + OpeningHours
				+ ", OpeningHoursComment=" + OpeningHoursComment
				+ ", HealthServicePhone=" + HealthServicePhone
				+ ", AppliesToCountyCodes=" + AppliesToCountyCodes
				+ ", AppliesToMunicipalityCodes="
				+ AppliesToMunicipalityCodes + "]";
	}

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	
	public String getHealthServiceDisplayName() {
		return HealthServiceDisplayName;
	}

	public void setHealthServiceDisplayName(String healthServiceDisplayName) {
		HealthServiceDisplayName = healthServiceDisplayName;
	}

	public String getHealthServiceLatitude() {
		return HealthServiceLatitude;
	}

	public void setHealthServiceLatitude(String healthServiceLatitude) {
		HealthServiceLatitude = healthServiceLatitude;
	}

	public String getHealthServiceLongitude() {
		return HealthServiceLongitude;
	}

	public void setHealthServiceLongitude(String healthServiceLongitude) {
		HealthServiceLongitude = healthServiceLongitude;
	}

	public String getVisitAddressPostName() {
		return VisitAddressPostName;
	}

	public void setVisitAddressPostName(String visitAddressPostName) {
		VisitAddressPostName = visitAddressPostName;
	}

	public String getVisitAddressPostNr() {
		return VisitAddressPostNr;
	}

	public void setVisitAddressPostNr(String visitAddressPostNr) {
		VisitAddressPostNr = visitAddressPostNr;
	}

	public String getVisitAddressStreet() {
		return VisitAddressStreet;
	}

	public void setVisitAddressStreet(String visitAddressStreet) {
		VisitAddressStreet = visitAddressStreet;
	}

	public String getHealthServiceWeb() {
		return HealthServiceWeb;
	}

	public void setHealthServiceWeb(String healthServiceWeb) {
		HealthServiceWeb = healthServiceWeb;
	}

	public String getOpeningHours() {
		return OpeningHours;
	}

	public void setOpeningHours(String openingHours) {
		OpeningHours = openingHours;
	}

	public String getOpeningHoursComment() {
		return OpeningHoursComment;
	}

	public void setOpeningHoursComment(String openingHoursComment) {
		OpeningHoursComment = openingHoursComment;
	}

	public String getHealthServicePhone() {
		return HealthServicePhone;
	}

	public void setHealthServicePhone(String healthServicePhone) {
		HealthServicePhone = healthServicePhone;
	}

	public String getAppliesToCountyCodes() {
		return AppliesToCountyCodes;
	}

	public void setAppliesToCountyCodes(String appliesToCountyCodes) {
		AppliesToCountyCodes = appliesToCountyCodes;
	}

	public String getAppliesToMunicipalityCodes() {
		return AppliesToMunicipalityCodes;
	}

	public void setAppliesToMunicipalityCodes(String appliesToMunicipalityCodes) {
		AppliesToMunicipalityCodes = appliesToMunicipalityCodes;
	}
	

}
