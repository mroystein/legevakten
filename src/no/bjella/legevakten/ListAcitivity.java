package no.bjella.legevakten;

import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

public class ListAcitivity extends Activity implements OnItemClickListener {

	private MyDbManager dbManager;
	private ListView listView;
	private List<Legevakt> list;
	
	private ArrayAdapter<Legevakt> adapter;
	
	 // Search EditText
   private  EditText inputSearch;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_list_acitivity);

		// tvStatus = (TextView) findViewById(R.id.textView1);
		dbManager = new MyDbManager(this);
		dbManager.open();

		list = dbManager.getALlLegevakter();

		listView = (ListView) findViewById(R.id.list_view_legevakter);

		adapter = new ArrayAdapter<Legevakt>(this,
				android.R.layout.simple_list_item_1, list);
		listView.setAdapter(adapter);

		listView.setOnItemClickListener(this);
		
		
		 inputSearch = (EditText) findViewById(R.id.inputSearch);
		
		 /**
         * Enabling Search Filter
         * */
        inputSearch.addTextChangedListener(new TextWatcher() {
 
            @Override
            public void onTextChanged(CharSequence cs, int arg1, int arg2, int arg3) {
                // When user changed the Text
                ListAcitivity.this.adapter.getFilter().filter(cs);
            }
 
            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
                    int arg3) {
                // TODO Auto-generated method stub
 
            }
 
            @Override
            public void afterTextChanged(Editable arg0) {
                // TODO Auto-generated method stub
            }
        });

	}

	@Override
	protected void onPause() {
		dbManager.close();
		super.onPause();
	}

	@Override
	protected void onDestroy() {
		dbManager.close();
		super.onDestroy();
	}

	@Override
	protected void onResume() {
		dbManager.open();
		super.onResume();
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.list_acitivity, menu);
		return true;
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		
		

		Legevakt l = adapter.getItem(position);

		int myId = list.get(position).getId();
		
	
		
		
		//showToastMessage("Str: "+l.getVisitAddressStreet()+", postnr:"+l.getVisitAddressPostNr()+", postname:"+ l.getVisitAddressPostName());
		
		Intent i = new Intent(this, LegevaktActivity.class);
		i.putExtra("name", l.getHealthServiceDisplayName());
		i.putExtra("lat", l.getHealthServiceLatitude());
		i.putExtra("long", l.getHealthServiceLongitude());
		i.putExtra("opnen", l.getOpeningHours());
		i.putExtra("phone", l.getHealthServicePhone());
		
		i.putExtra("addr_postname", l.getVisitAddressPostName());
		i.putExtra("addr_postnr", l.getVisitAddressPostNr());
		i.putExtra("addr_street", l.getVisitAddressStreet());
		
		startActivity(i);

	}
	public void showToastMessage(String msg) {
		Toast.makeText(this, msg, Toast.LENGTH_LONG).show();
	}

}
