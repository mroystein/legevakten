package no.bjella.legevakten;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class LegevaktActivity extends Activity {

	private float latPos, longPos;
	private MyDbManager dbManager;
	private Legevakt legevakt;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_legevakt3);

		dbManager = new MyDbManager(this);
		dbManager.open();

		Bundle b = getIntent().getExtras();
		if (b != null) {
			Legevakt l = dbManager.getLegevaktByName(b.getString("name"));
			newSetup(l);
			legevakt=l;
		}

	}

	private void newSetup(Legevakt l) {
		setTitle(l.getHealthServiceDisplayName());

		OpeningHours oh = new OpeningHours();
		TextView tv = (TextView) findViewById(R.id.tvLegeAapning);
		tv.setText(oh.parseOpeningHours(l.getOpeningHours()));
		String openComment = l.getOpeningHoursComment();
		if (openComment != null && !openComment.equals("")) {
			tv.append(openComment);
		}

		tv = (TextView) findViewById(R.id.tvLegeTelefon);
		tv.setText(l.getHealthServicePhone());
		// tv.setText("55311806");

		// Toast.makeText(this,
		// "Str:"+addr_street+", postnr:"+addr_postnr+", name:"+addr_name,
		// Toast.LENGTH_LONG).show();

		tv = (TextView) findViewById(R.id.tvLegeAddr1);
		tv.setText("Street: " + l.getVisitAddressStreet());

		tv = (TextView) findViewById(R.id.tvLegeAddr2);
		tv.setText("Postnr: " + l.getVisitAddressPostNr() + " "
				+ l.getVisitAddressPostName());

		String countryCode = l.getAppliesToCountyCodes();
		if (countryCode != null && countryCode != "") {
			String fylke = fylkeCodeTilNavn(countryCode);
			tv.append("\nFylke: " + fylke);
		}

		// tv = (TextView) findViewById(R.id.tvDebug);

		// tv.setText(l.toString2());

		try {
			latPos = Float.parseFloat(l.getHealthServiceLatitude());
			longPos = Float.parseFloat(l.getHealthServiceLongitude());
		} catch (Exception e) {
			latPos = 0;
			longPos = 0;
			Button b = (Button) findViewById(R.id.buttonVisKart);
			// b.setEnabled(false);
			b.setVisibility(View.INVISIBLE);
			
			b = (Button) findViewById(R.id.buttonVisDirection);
			b.setVisibility(View.INVISIBLE);

		}

	}

	public void startKart(View v) {
		if (longPos == 0 || latPos == 0) {
			Toast.makeText(this, "Mangler informasjon om posisjon",
					Toast.LENGTH_SHORT).show();
		} else {
			Intent i = new Intent(this, MapActivity.class);
			i.putExtra("latPos", latPos);
			i.putExtra("longPos", longPos);
			startActivity(i);

		}

	}

	public void startGoogleMapDirection(View v) {
		// geo:latitude,longitude
		//String name = "Bergen legekontor";
		Intent intent = new Intent(android.content.Intent.ACTION_VIEW,
				Uri.parse("geo:0,0?q="+latPos+","+longPos+" (" + legevakt.getHealthServiceDisplayName() + ")"));
		startActivity(intent);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.legevakt, menu);
		return true;
	}

	private String fylkeCodeTilNavn(String code) {
		int n = 0;
		try {
			n = Integer.parseInt(code);
		} catch (Exception e) {
			return "";
		}

		switch (n) {
		case 1:
			return "�stfold";
		case 2:
			return "Akershus";
		case 3:
			return "Oslo";
		case 4:
			return "Hedmark";
		case 5:
			return "Oppland";
		case 6:
			return "Buskerud";
		case 7:
			return "Vestfold";
		case 8:
			return "Telemark";
		case 9:
			return "Aust-Agder";
		case 10:
			return "Vest-Agder";
		case 11:
			return "Rogaland";
		case 12:
			return "Hordaland";
		case 13:
			return "";
		case 14:
			return "Sogn og Fjordane";
		case 15:
			return "M�re og Romsdal";
		case 16:
			return "S�r-Tr�ndelag";
		case 17:
			return "Nord-Tr�ndelag";
		case 18:
			return "Nordland";
		case 19:
			return "Troms";
		case 20:
			return "Finnmark";
		case 21:
			return "Svalbard";
		default:
			return "Ikke spesifisert";

		}

	}
	
	@Override
	protected void onPause() {
		dbManager.close();
		super.onPause();
	}

	@Override
	protected void onDestroy() {
		dbManager.close();
		super.onDestroy();
	}

	@Override
	protected void onResume() {
		dbManager.open();
		super.onResume();
	}

}
