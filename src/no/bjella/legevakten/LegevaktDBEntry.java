package no.bjella.legevakten;

import android.provider.BaseColumns;

public class LegevaktDBEntry implements BaseColumns{
	 public static final String TABLE_NAME = "legevakter";
	   
	    public static final String COLUMN_NAME_NAME= "name";
	    public static final String COLUMN_NAME_LATITUDE = "latitude";
	    
	    public static final String COLUMN_NAME_LONGITUDE= "longitude";
	    public static final String COLUMN_NAME_OPENINGHOURS = "openinghours";
	    public static final String COLUMN_NAME_PHONE = "phone";
	    
	    
	    //NEW
	    public static final String COLUMN_NAME_ADDR_POSTNAME = "postname";
	    public static final String COLUMN_NAME_ADDR_POSTNR = "postnr";
	    public static final String COLUMN_NAME_ADDR_STREET = "street";
	    public static final String COLUMN_NAME_WEB = "web";
	    
	
	    public static final String COLUMN_NAME_OPENING_COMMENT = "comment";
	    public static final String COLUMN_NAME_COUNTRYCODE = "countrycode";
	    public static final String COLUMN_NAME_MUNICODE = "municode";
	    
	    
	
	   
	    
	 // Prevents the FeedReaderContract class from being instantiated.
	    private LegevaktDBEntry() {}
	    
		/*
		
		*/
		
		
		
		
	    
	    
	    

}
