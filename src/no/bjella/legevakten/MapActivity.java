package no.bjella.legevakten;

import java.util.List;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnInfoWindowClickListener;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;



public class MapActivity extends FragmentActivity implements
		OnInfoWindowClickListener {

	
	private GoogleMap mMap;
	private MyDbManager dbManager;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_map);
		setUpMapIfNeeded();

		dbManager = new MyDbManager(this);
		dbManager.open();

		List<Legevakt> list = dbManager.getALlLegevakter();
		int teller = 0;
		for (Legevakt b : list) {
			try {
				makeMarker(b);
			} catch (Exception e) {
				teller++;
			}
		}

		mMap.setOnInfoWindowClickListener(this);

		Toast.makeText(this, teller + " ble ikke lagt til pga manglene info",
				Toast.LENGTH_SHORT).show();

		Bundle b = getIntent().getExtras();
		if (b != null) {
			float latPos = b.getFloat("latPos");
			float longPos = b.getFloat("longPos");

			CameraUpdate center = CameraUpdateFactory.newLatLng(new LatLng(
					latPos, longPos));
			CameraUpdate zoom=CameraUpdateFactory.zoomTo(15);

		    mMap.moveCamera(center);
		    mMap.animateCamera(zoom);
		}

	}

	private void makeMarker(Legevakt b) {
		OpeningHours oh = new OpeningHours();
		String aapningstid = oh.parseOpeningHours(b.getOpeningHours());

		String s = aapningstid;
		String latStr = b.getHealthServiceLatitude();
		String longStr = b.getHealthServiceLongitude();

		double latPos = Double.parseDouble(latStr);
		double longPos = Double.parseDouble(longStr);

		LatLng latLng = new LatLng(latPos, longPos);
		mMap.addMarker(new MarkerOptions()
				.position(latLng)
				.title(b.getHealthServiceDisplayName())
				.snippet(s)
				.icon(BitmapDescriptorFactory
						.defaultMarker(BitmapDescriptorFactory.HUE_ROSE)));
	}

	@Override
	protected void onResume() {
		super.onResume();
		dbManager.open();
		setUpMapIfNeeded();
		
	}

	private void setUpMapIfNeeded() {

		// Do a null check to confirm that we have not already instantiated the
		// map.
		if (mMap == null) {

			Fragment fragment = getSupportFragmentManager().findFragmentById(
					R.id.map);
			SupportMapFragment mapFragment = (SupportMapFragment) fragment;
			mMap = mapFragment.getMap();
			// mMap = ((MapFragment)
			// getFragmentManager().findFragmentById(R.id.map)).getMap();
			// Check if we were successful in obtaining the map.
			if (mMap != null) {
				// The Map is verified. It is now safe to manipulate the map.
				Log.d(TAG, "MAP ble satt opp riktig");
			} else {
				Log.d(TAG, "MAP ble satt opp FEIL!!!");
			}
		}
	}

	String TAG = "MyMapAcitvity";

	@Override
	public void onInfoWindowClick(Marker m) {

		//Toast.makeText(this, m.getTitle(), Toast.LENGTH_SHORT).show();

		Legevakt l = dbManager.getLegevaktByName(m.getTitle());
		// Intent i = new Intent(this, BomsatsjonViewActivity.class);
		// i.putExtra("navn", m.getTitle());
		// startActivity(i);

		Intent i = new Intent(this, LegevaktActivity.class);
		i.putExtra("name", l.getHealthServiceDisplayName());
		i.putExtra("lat", l.getHealthServiceLatitude());
		i.putExtra("long", l.getHealthServiceLongitude());
		i.putExtra("opnen", l.getOpeningHours());
		i.putExtra("phone", l.getHealthServicePhone());

		i.putExtra("addr_postname", l.getVisitAddressPostName());
		i.putExtra("addr_postnr", l.getVisitAddressPostNr());
		i.putExtra("addr_street", l.getVisitAddressStreet());

		startActivity(i);

	}
	
	@Override
	protected void onPause() {
		dbManager.close();
		super.onPause();
	}

	@Override
	protected void onDestroy() {
		dbManager.close();
		super.onDestroy();
	}



}
