package no.bjella.legevakten;

import java.util.ArrayList;



public class OpeningHours {
	private Day[] days;
	private static final int ONEDAY = 60*24;
	private static final int FOURHOURS = 60*4;
	private static final int FIFTEENHOURS = 60*15;
	private static final int HOUR = 60;
	
	public OpeningHours() {
		clear();
	}

	private void clear() {
		days = new Day[7];
		for(int i = 0; i < days.length; i++)
			days[i] = new Day(i);
	}

	public String parseOpeningHours(String parseStr) {
		String[] times = parseStr.split(",");
		for(int i = 0; i < times.length; i++) {
			String[] twoTimes = times[i].split("-");
			int start = Integer.parseInt(twoTimes[0].trim());
			int end = Integer.parseInt(twoTimes[1].trim());
			fillTime(start,end);
			
			//Kjapp fiks ; 0-10080 -> 
			//if(start==0 && end>=10080){
			//	return "D�gn�pent alle dager.";
			//}
		}
		
//		for(int i = 0; i < days.length; i++) {
//			if(days[i].isValidDay())
//				System.out.println(days[i]);
//		}
//		System.out.println();
		
		// check for days in a row with same times
		ArrayList<Day> finalDays = new ArrayList<Day>();
		boolean checkedDays[] = new boolean[7];
		for(int i = 0; i < days.length-2; i++) {
			if(checkedDays[i])
				continue;
			if(days[i].endTime>=ONEDAY*3)
				continue;
			int equalStreak = 0;
			int last = 0;
			for(int j = i+1; j < days.length; j++) {
				if(!checkedDays[i] && days[i].equals(days[j])) {
					equalStreak++;
					last = j;
				}
				else
					break;
			}
			if(equalStreak>=2) {
				checkedDays[i] = true;
				Day d = new Day(i);
				d.endDay = days[last].endDay;
				d.startTime = days[i].startTime;
				d.endTime = days[i].endTime;
				for(int a = i+1; a <= last; a++) {
					d.endTime+=ONEDAY;
					checkedDays[a] = true;
				}
				finalDays.add(d);
			} else {
				if(days[i].isValidDay()) {
					checkedDays[i] = true;
					finalDays.add(days[i]);
				}
			}
		}
		
		Day weekend = null;
		// weekends 24/7
		if(days[4].isValidDay() && !checkedDays[4]) {
			int start = days[4].startTime;
			int end = days[4].endTime;
			if(start > FIFTEENHOURS && start < ONEDAY && end > ONEDAY*3) {
				weekend = new Day(4);
				weekend.out = "Weekend 24/7";
				for(int i = 4; i < days.length; i++)
					checkedDays[i] = true;
			}
		}
		
		for(int i = 0; i < days.length; i++) {
			if(!checkedDays[i] && days[i].isValidDay())
				finalDays.add(days[i]);
		}
		
		if(weekend != null)
			finalDays.add(weekend);
		
		String s = printDays2(finalDays);
		
		// clear
		clear(); 
		return s;
	}

	private String printDays2(ArrayList<Day> days) {
		String s ="";
		for(Day d : days){
			//Log.d("OpeningHours", d.toString());
			s+=d+"\n";
		}
			
		return s;
	}

	
	private void printDays(ArrayList<Day> days) {
		System.out.println();
		for(Day d : days)
			System.out.println(d);
		System.out.println();
	}

	private void fillTime(int startTime, int endTime) {
		
		for(int i = 0; i < days.length; i++) {
			int start = startTime - ONEDAY*i;
			int end = endTime - ONEDAY*i;
			if(start >= 0 && start < ONEDAY) {
				days[i].startTime = Math.min(days[i].startTime, start);
				if(end > days[i].endTime) {					
					days[i].endTime = end;
					days[i].endDay = i;
				}
				int temp = end;
				for(int j = i+1; j < days.length; j++) {
					temp-=ONEDAY;
					if(temp > 0) {
						days[i].endDay++;
						days[j].endTime = Math.max(days[j].endTime, temp);
					}
				}
			}
		}
		
	}
}

class Day {
	private static final int ONEDAY = 60*24;
	private static final int FOURHOURS = 60*4;
	String dayStr;
	int startTime,endTime;
	int startDay,endDay;
	int startHour,endHour;
	int startMinute,endMinute;
	String out;
	String[] days = { "Mandag", "Tirsdag", "Onsdag", "Torsdag", "Fredag", "L�rdag", "S�ndag", "UNKNOWN"};
	
	public Day(int dayNr) {
		startTime = Integer.MAX_VALUE;
		endTime = Integer.MIN_VALUE;
		startDay = dayNr;
		endDay = 7;
		startHour = endHour = startMinute = endMinute = 0;
		out = "null";
	}

	private void convert() {
		if(startDay==6 && endTime > ONEDAY)
			endDay = 7;
		startHour = startTime/60;
		startMinute = startTime%60;
		
		endHour = (endTime-(ONEDAY*(endDay-startDay)))/60;
		endMinute = (endTime-(ONEDAY*(endDay-startDay)))%60;
		if(startDay==6 && endTime > ONEDAY)
			endDay = 0;
	}
	
	public String getTime(int start, int end) {
		String hour = start+"";
		String minute = end+"";
		
		if(hour.length()<2)
			hour = "0"+hour;
		if(minute.length()<2)
			minute = "0"+minute;
		
		return hour+":"+minute;
	}
	
	public String getStartTime() {
		return getTime(startHour,startMinute);
	}
	
	public String getEndTime() {
		return getTime(endHour,endMinute);
	}
	
	public boolean isValidDay() {
		return startTime != Integer.MAX_VALUE && endTime != Integer.MIN_VALUE && startTime<endTime;
	}
	
	public String toString() {
		convert();
		if(!out.equals("null"))
			return out;
		if(startDay == 0 && endDay == 6) {
			if(endHour>24)
				endHour-=24;
			if(startTime==0 && endTime >= ONEDAY*7-1)
				return "D�gn�pent alle dager.";
			return "Alle dager " + getStartTime() + " - " + getEndTime();
		}
//		if(startTime==0 && endTime >= ONEDAY-1 && endTime <= ONEDAY+FOURHOURS)
//			return days[startDay] + " 24/7";
		if(endTime<ONEDAY+FOURHOURS)
			return days[startDay] + " " + getStartTime() + " - " + getEndTime();
		if(endTime>=ONEDAY*3)
			return days[startDay] + " til " + days[endDay] + " " + getStartTime() + " - " + getEndTime();
		return days[startDay] + " " + getStartTime() + " til "  + days[endDay] + " " + getEndTime();
	}
	
	public boolean equals(Day d) {
		return startTime == d.startTime && endTime == d.endTime && d.isValidDay();
	}

}