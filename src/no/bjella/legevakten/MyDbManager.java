package no.bjella.legevakten;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public class MyDbManager {
	private MyDBHelper myDbHelper;
	private SQLiteDatabase db;

	private String[] allColumns = { LegevaktDBEntry._ID,
			LegevaktDBEntry.COLUMN_NAME_NAME,
			LegevaktDBEntry.COLUMN_NAME_LATITUDE,
			LegevaktDBEntry.COLUMN_NAME_LONGITUDE,
			LegevaktDBEntry.COLUMN_NAME_OPENINGHOURS,
			LegevaktDBEntry.COLUMN_NAME_PHONE,
			LegevaktDBEntry.COLUMN_NAME_ADDR_POSTNAME,
			LegevaktDBEntry.COLUMN_NAME_ADDR_POSTNR,
			LegevaktDBEntry.COLUMN_NAME_ADDR_STREET,
			LegevaktDBEntry.COLUMN_NAME_WEB,
			LegevaktDBEntry.COLUMN_NAME_OPENING_COMMENT,
			LegevaktDBEntry.COLUMN_NAME_COUNTRYCODE,
			LegevaktDBEntry.COLUMN_NAME_MUNICODE };

	public MyDbManager(Context context) {

		myDbHelper = new MyDBHelper(context);

	}

	public void open() {
		db = myDbHelper.getWritableDatabase();
	}

	public void close() {
		myDbHelper.close();
	}

	public void deleteAndUpdateDb() {
		myDbHelper.onUpgrade(db, 0, 0);
	}

	public Legevakt addNewLegevakt(Legevakt l) {

		// Create a new map of values, where column names are the keys
		ContentValues values = new ContentValues();
		// values.put(LegevaktDBEntry._ID, l.getId());
		values.put(LegevaktDBEntry.COLUMN_NAME_NAME,
				test(l.getHealthServiceDisplayName()));
		values.put(LegevaktDBEntry.COLUMN_NAME_LATITUDE,
				test(l.getHealthServiceLatitude()));
		values.put(LegevaktDBEntry.COLUMN_NAME_LONGITUDE,
				test(l.getHealthServiceLongitude()));
		values.put(LegevaktDBEntry.COLUMN_NAME_OPENINGHOURS,
				test(l.getOpeningHours()));
		values.put(LegevaktDBEntry.COLUMN_NAME_PHONE,
				test(l.getHealthServicePhone()));

		values.put(LegevaktDBEntry.COLUMN_NAME_ADDR_POSTNAME,
				test(l.getVisitAddressPostName()));

		values.put(LegevaktDBEntry.COLUMN_NAME_ADDR_POSTNR,
				test(l.getVisitAddressPostNr()));
		values.put(LegevaktDBEntry.COLUMN_NAME_ADDR_STREET,
				test(l.getVisitAddressStreet()));
		values.put(LegevaktDBEntry.COLUMN_NAME_WEB,
				test(l.getHealthServiceWeb()));

		values.put(LegevaktDBEntry.COLUMN_NAME_OPENING_COMMENT,
				test(l.getOpeningHoursComment()));
		values.put(LegevaktDBEntry.COLUMN_NAME_COUNTRYCODE,
				test(l.getAppliesToCountyCodes()));

		values.put(LegevaktDBEntry.COLUMN_NAME_MUNICODE,
				test(l.getAppliesToMunicipalityCodes()));

		// Insert the new row, returning the primary key value of the new row
		long newRowId;
		newRowId = db.insert(LegevaktDBEntry.TABLE_NAME,
				LegevaktDBEntry.COLUMN_NAME_MUNICODE, values);

		int t = (int) newRowId;
		l.setId(t);
		return l;
	}
	
	public int addNewLegevakt2(Legevakt l) {

		// Create a new map of values, where column names are the keys
		ContentValues values = new ContentValues();
		// values.put(LegevaktDBEntry._ID, l.getId());
		values.put(LegevaktDBEntry.COLUMN_NAME_NAME,
				test(l.getHealthServiceDisplayName()));
		values.put(LegevaktDBEntry.COLUMN_NAME_LATITUDE,
				test(l.getHealthServiceLatitude()));
		values.put(LegevaktDBEntry.COLUMN_NAME_LONGITUDE,
				test(l.getHealthServiceLongitude()));
		values.put(LegevaktDBEntry.COLUMN_NAME_OPENINGHOURS,
				test(l.getOpeningHours()));
		values.put(LegevaktDBEntry.COLUMN_NAME_PHONE,
				test(l.getHealthServicePhone()));

		values.put(LegevaktDBEntry.COLUMN_NAME_ADDR_POSTNAME,
				test(l.getVisitAddressPostName()));

		values.put(LegevaktDBEntry.COLUMN_NAME_ADDR_POSTNR,
				test(l.getVisitAddressPostNr()));
		values.put(LegevaktDBEntry.COLUMN_NAME_ADDR_STREET,
				test(l.getVisitAddressStreet()));
		values.put(LegevaktDBEntry.COLUMN_NAME_WEB,
				test(l.getHealthServiceWeb()));

		values.put(LegevaktDBEntry.COLUMN_NAME_OPENING_COMMENT,
				test(l.getOpeningHoursComment()));
		values.put(LegevaktDBEntry.COLUMN_NAME_COUNTRYCODE,
				test(l.getAppliesToCountyCodes()));

		values.put(LegevaktDBEntry.COLUMN_NAME_MUNICODE,
				test(l.getAppliesToMunicipalityCodes()));

		// Insert the new row, returning the primary key value of the new row
		long newRowId;
		newRowId = db.insert(LegevaktDBEntry.TABLE_NAME,
				LegevaktDBEntry.COLUMN_NAME_MUNICODE, values);

		int t = (int) newRowId;
		l.setId(t);
		return t;
	}

	private String test(String s) {
		if (s == null) {
			s = "";
		}
		return s;
	}

	public List<Legevakt> getALlLegevakter() {
		List<Legevakt> list = new ArrayList<Legevakt>();
		Cursor cursor = db.query(LegevaktDBEntry.TABLE_NAME, allColumns, null,
				null, null, null, null);

		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			Legevakt l = cursorToLegevakt(cursor);
			list.add(l);
			cursor.moveToNext();
		}
		// Make sure to close the cursor
		cursor.close();
		return list;
	}
	
	public Legevakt getLegevaktByName(String name) {
		Legevakt l = null;
		//String select = LegevaktDBEntry.COLUMN_NAME_NAME"=?";
		
	
		
		  Cursor cursor = db.query(LegevaktDBEntry.TABLE_NAME, allColumns, LegevaktDBEntry.COLUMN_NAME_NAME + " = '"+
		            name+"'", null, null, null, null);

		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			l = cursorToLegevakt(cursor);
			cursor.moveToNext();
		}
		// Make sure to close the cursor
		cursor.close();
		
		return l;
	}

	private Legevakt cursorToLegevakt(Cursor cursor) {
		Legevakt l = new Legevakt();

		l.setId(cursor.getInt(0));
		l.setHealthServiceDisplayName(cursor.getString(1));
		l.setHealthServiceLatitude(cursor.getString(2));
		l.setHealthServiceLongitude(cursor.getString(3));
		l.setOpeningHours(cursor.getString(4));
		l.setHealthServicePhone(cursor.getString(5));

		
		
		l.setVisitAddressPostName(cursor.getString(6));
		l.setVisitAddressPostNr(cursor.getString(7));
		l.setVisitAddressStreet(cursor.getString(8));

		l.setHealthServiceWeb(cursor.getString(9));

		l.setOpeningHoursComment(cursor.getString(10));
		l.setAppliesToCountyCodes(cursor.getString(11));
		
		
		l.setAppliesToMunicipalityCodes(cursor.getString(12));

		return l;
	}

}
