package no.bjella.legevakten;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.format.Time;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;

public class MainActivity extends Activity {
	TextView tvStatus;

	static String urlAll = "http://data.helsenorge.no/KeywordQuerySearch.svc/?k=KA02&$top=500&s=HelsetjenesterBDC&$select=HealthServiceDisplayName,HealthServiceLatitude,HealthServiceLongitude,VisitAddressPostName,VisitAddressPostNr,VisitAddressStreet,HealthServiceWeb,OpeningHours,OpeningHoursComment,HealthServicePhone,AppliesToCountyCodes,AppliesToMunicipalityCodes";
	static String url3 = "http://data.helsenorge.no/KeywordQuerySearch.svc/?k=KA02&$top=500&s=HelsetjenesterBDC&$select=HealthServiceDisplayName,HealthServiceLatitude,HealthServiceLongitude,VisitAddressPostName,VisitAddressPostNr,VisitAddressStreet,HealthServiceWeb,OpeningHours,OpeningHoursComment,HealthServicePhone,AppliesToCountyCodes,AppliesToMunicipalityCodes";
	String urlNew = "http://www.newcastle.edu.au/";

	MyDbManager dbManager;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.acitivty_main2);

		tvStatus = (TextView) findViewById(R.id.textViewMainStatus);
		SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
		String dato = settings.getString("db_date", "Ingen db");

		dbManager = new MyDbManager(this);
		dbManager.open();

		if (dato.equals("Ingen db")) {

			LoadJsonFromAsset loadJson = new LoadJsonFromAsset();
			loadJson.execute("");

			dato = settings.getString("db_date", "Ingen db");

		}

		updateTextViewStatus();

	}

	private void updateTextViewStatus() {
		SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
		String dato = settings.getString("db_date", "Ingen db");
		tvStatus.setText("Databasen inneholder "
				+ dbManager.getALlLegevakter().size()
				+ " legevakter. Lastet ned den " + dato);
	}

	@Override
	protected void onPause() {
		dbManager.close();
		super.onPause();
	}

	@Override
	protected void onDestroy() {
		dbManager.close();
		super.onDestroy();
	}

	@Override
	protected void onResume() {
		dbManager.open();
		super.onResume();
	}

	public void onClickDownload(View view) {
		// Toast.makeText(this, "Hey", Toast.LENGTH_SHORT).show();
		DownloadFile downloadFile = new DownloadFile();
		downloadFile.execute(url3);

	}

	public void startMap(View v) {
		startMapActivity();
	}

	public void startList(View v) {
		startListActivity();

	}

	public void showToastMessage(String msg) {
		Toast.makeText(this, msg, Toast.LENGTH_LONG).show();
	}

	private void startMapActivity() {
		Intent i = new Intent(this, MapActivity.class);
		startActivity(i);
	}

	private void startListActivity() {
		Intent i = new Intent(this, ListAcitivity.class);
		startActivity(i);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.map_acitivity:
			startMapActivity();
			break;

		case R.id.action_list:
			startListActivity();
			break;
		default:
			break;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.main, menu);
		return true;
	}

	public void svaveJSONStringToDb(String result, boolean fromAssets) {

		Gson gson = new Gson();
		ResultList r = gson.fromJson(result, ResultList.class);
		// showToastMessage(r.toString());
		List<Legevakt> list = r.getRelevantResults();

		dbManager.deleteAndUpdateDb();
		for (Legevakt l : list) {
			l = dbManager.addNewLegevakt(l);
		}

		// tvStatus.setText(list.size() + " legevakter finnes i databasen");

		String mDate;
		if (fromAssets) {
			mDate = "2013-2-10 1:11:6"; // assetDate
		} else {
			Time t = new Time();
			t.setToNow();

			mDate = String.format("%s-%s-%s %s:%s:%s", t.year, t.month,
					t.monthDay, t.hour, t.minute, t.second);

		}
		SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
		SharedPreferences.Editor editor = settings.edit();
		editor.putString("db_date", mDate);
		editor.commit();
		updateTextViewStatus();

	}

	String PREFS_NAME = "Legevakten";

	static class ResultList {
		List<Legevakt> RelevantResults;

		public List<Legevakt> getRelevantResults() {
			return RelevantResults;
		}

		public void setRelevantResults(List<Legevakt> relevantResults) {
			RelevantResults = relevantResults;
		}

		@Override
		public String toString() {
			return "Size : " + RelevantResults.size();
		}
	}

	private class DownloadFile extends AsyncTask<String, Integer, String> {

		ProgressDialog progressDialog;

		@Override
		protected void onPreExecute() {
			progressDialog = ProgressDialog.show(MainActivity.this,
					"Laster ned data",
					"Alle legevaktene vil bli lagret lokalt (ca 127 KB)", true);
			super.onPreExecute();
		}

		@Override
		protected String doInBackground(String... sUrl) {

			StringBuilder builder = new StringBuilder();
			HttpClient client = new DefaultHttpClient();
			HttpGet httpGet = new HttpGet(sUrl[0]);

			try {
				HttpResponse response = client.execute(httpGet);

				StatusLine statusLine = response.getStatusLine();
				int statusCode = statusLine.getStatusCode();
				if (statusCode == 200) {
					HttpEntity entity = response.getEntity();
					InputStream content = entity.getContent();
					BufferedReader reader = new BufferedReader(
							new InputStreamReader(content, "UTF-8"));
					String line;
					while ((line = reader.readLine()) != null) {
						builder.append(line);
					}
				}

			} catch (Exception e) {
				e.printStackTrace();
			}
			return builder.toString();
		}

		@Override
		protected void onProgressUpdate(Integer... values) {
			// TODO Auto-generated method stub
			super.onProgressUpdate(values);
		}

		@Override
		protected void onPostExecute(String result) {

			svaveJSONStringToDb(result, false);
			progressDialog.dismiss();
			// showToastMessage(result);
			// tvStatus.setText(result);
			// tvStatus.setText("Lenght :"+result.length());
		}

	}

	private class LoadJsonFromAsset extends AsyncTask<String, Integer, String> {

		ProgressDialog progressDialog;

		@Override
		protected void onPreExecute() {
			progressDialog = ProgressDialog
					.show(MainActivity.this,
							"Setter opp databasen",
							"Henter data lokalt. Dette skjer bare f�rste gang applikasjonen kj�res",
							true);
			super.onPreExecute();
		}

		@Override
		protected String doInBackground(String... sUrl) {

			StringBuilder json = new StringBuilder();
			try {
				InputStream is = getAssets().open("legevakter.json");

				BufferedReader br = new BufferedReader(new InputStreamReader(
						is, "UTF-8"));
				String line = null;

				while ((line = br.readLine()) != null) {
					json.append(line);

				}
				br.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return json.toString();
		}

		@Override
		protected void onProgressUpdate(Integer... values) {
			// TODO Auto-generated method stub
			super.onProgressUpdate(values);
		}

		@Override
		protected void onPostExecute(String result) {

			svaveJSONStringToDb(result, true);
			progressDialog.dismiss();
			// showToastMessage(result);
			// tvStatus.setText(result);
			// tvStatus.setText("Lenght :"+result.length());
		}

	}

}
